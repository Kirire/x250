#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Petit script de lancement du serveur pour la doc Sphinx

@author: Cyrile Delestre
"""

import http.server
import socketserver

PORT = 8080
DIRECTORY = "build/html/"

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    # lancement du serveur
    httpd.serve_forever()

