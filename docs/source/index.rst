.. DSTK documentation master file, created by
   sphinx-quickstart on Sun Nov 22 14:18:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation du package DSTK (Data Science ToolKit) !
========================================================================

.. image:: ./_static/dstk.png
    :align: center
    :width: 500

**DSTK** est une libraire haut-niveau pour faciliter le développement et le déploiment d'outil de machine learning. Il s'articule essentiellement autour de 2 outils :
    * PyTorch ;
    * Scikit-Learn.

L'ancien nom x250 faisait référence au code boite X250 qui est le code boite de datalabs/IA Factory. Le développement de ce package est étroitement lié au développement du template data science (qui aujourd'hui a fusionné avec le socle Python).

**DTSK** est compatible avec `Python >= 3.5`, mais `Python >= 3.8` est fortement recommandé.

Documentation des fonctionnalités
=================================

.. toctree::
    :maxdepth: 2
    :caption: Fonctionnalités techniques

    metrics
    ssh
    data

.. toctree::
    :maxdepth: 2
    :caption: PyTorch

    pytorch

.. toctree::
    :maxdepth: 2
    :caption: Fonctionnalités data science

    features
    ml
    visualization

.. toctree::
    :maxdepth: 2
    :caption: Utilitaires

    utils

.. toctree::
    :maxdepth: 2
    :caption: Guide

    help

Index et tables
===============

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
