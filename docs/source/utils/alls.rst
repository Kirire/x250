Tous les utilitaires
====================

.. automodule:: dstk.utils._set_deep_params
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._check
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._chunk
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._rolling
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._trie
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._date
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._transpose
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._statistique
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._seed
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils._timeout
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils.deprecated
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: dstk.utils.errors
    :members:
    :undoc-members:
    :show-inheritance:


