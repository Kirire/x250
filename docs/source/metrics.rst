**metrics** c'est quoi ?
========================
La librairie dstk.metrics permet la gestion d'envoi de métriques (:class:`~dstk.metrics._metrics`). Afin de créer un envoi performant et asynchrone du reste des programmes du projet, le module s'appuit sur les méthodes de loggers de Python. Une implémentation simple par décorateur a été préférée afin de garantir la flexibilité d'utilisation et d'implémentation du module. Il est possible d'envoyer deux types de mesure :
    - le temps d'exécution de la fonction, méthode ou classe ;
    - la sortie d'une fonction ou méthode à l'aide de la classe :class:`~dstk.metrics._aggregator.Aggregator`.
Ces métriques peuvent être complétées avec le décorateur :func:`~dstk.utils._timeout.timeout` implémenté dans les utilitaires.
Les métriques générées sont envoyées vers un support (:class:`~dstk.metrics._handlers`) de donnée. Ces supports peuvent être de différents types :
    - sortie standard ;
    - fichier texte ;
    - base SQLite ;
    - base MySQL, Oracle, etc. ;
    - Warp10 ;
    - Hartimon (gestion des métriques Warp10 d'Arkéa) ;
    - *etc.*

.. warning:: La classe :class:`~dstk.metrics._aggregator.Aggregator` est beaucoup plus simple d'utilisation avec Python >= 3.8.

**metrics** : classes et fonctions
==================================

.. toctree::
   :maxdepth: 2

   metrics/metrics
   metrics/aggregator
   metrics/handlers

