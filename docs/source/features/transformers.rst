Facilitateur de création de wrapper Scikit-Learn Transformers
=============================================================

.. automodule:: dstk.features._transformers
   :members:
   :undoc-members:
   :show-inheritance:

