Besoin d'aide ?
===============

Si vous rencontrez des problèmes contacter : cyrile.urf.orsay@gmail.com

Informations sur les versions
=============================

**Les release notes sont scrupuleusement notées que depuis la version 3.3.**

### 3.0

    * Changement de nom, **la librairie x250 devient DSTK** afin d'être rendu publique sur PyPi dans un premiet temps et sur conda dans un second.
    * 3.3 (05/04/2021) :
        * changement dans les classes :class:`~dstk.metrics._handlers.SQLiteHandler`: et :class:`~dstk.metrics._handlers.SQLAlchemyHandler` afin de les rendres compatibles multi-thread.
    * 3.4 (14/05/2021) :
        * Changement dans les requirements sklearn -> scikit-learn.
    * 3.5 (18/05/2021) :
        * :meth:`~dstk.pytorch._base.BaseEnvironnement.fit` : rectification du bug de récupération du nom sur la fonction coût est un classe PyTorch plutôt qu'une fonction coût PyTorch.
        * La fonction :func:`~dstk.pytorch._utils.check_tensor` accepte à présent des scalaires de type Numpy (np.integer et np.floating) à l'instart des type primitif Python float et int.
        * Création de la classe :class:`~dstk.utils._rooling.RollingWindow` qui permet de faire un rolling sur un itérateur qui peut être une DataFrame Pandas, un array Numpy ou un liste.
    * 3.6 (17/10/2021) :
        * La partie **SWA ne fonctionne plus sur cette version !**
        * Changement dans le comportement :class:`~dstk.metrics._handlers.ArtimonLocalHandler` permettant de stocker les métriques temporairement dans un répertoire local pour qu'un programme interne aux serveurs de prod gère lui même les envoies dans Artimon. Ecriture dans un fichier temporaire "XXX.tmp" pour être renomé "XXX.artimon" une fois fini. Le gestion des fichier n'est plus géré par la classe.
        * Création des couches :class:`~dstk.pytorch.networks.MaskedLinear` designé pour PyTorch vanilla, :class:`~dstk.pytorch.networks.RisudualNonLinearLayer` et :class:`~dstk.pytorch.networks.Autoregressive` orienté time series.
        * Création de la fonction :func:`~dstk.pytorch._utils.make_mask` permettant de générer un masque pour designer une couche de neuronne linéaire (fonctionne pour :class:`~dstk.pytorch.networks.MaskedLinear`).
        * La fonction :func:`~dstk.pytorch._utils.check_tensor_dict` accepte à présent des scalaires de type Numpy (np.integer et np.floating) à l'instart des type primitif Python float et int.
        * Rectification des bugs sur la classe :class:`~dstk.utils._rooling.RollingWindow` permettant à présent de faire une window size de 1 + conservation des indexes si une DataFrame Pandas.
        * Modification de la fonction :func:`~dstk.ssh._utils.download_file` sur la partie affichage de la quantité de donnée sur TQDM ainsi qu'une meilleur gestion des paramètres envoyé dans l'URL et dans le header.
        * Rectification du bug de la fonction :func:`~dstk.utils._statistique.weighted_avg_and_std` qui empêchait l'utilisation des matrices de dimension supérieur à 2.
        * Le callback :class:`~dstk.pytorch._callback.LRSchedulerCallback` intègre aprésent la possibilité la possiblité de faire intervenir le scheduler durant la phase d'apprentissage via l'argument **by_step=True**.
        * Création du callback :class:`~dstk.pytorch._callback.Backup` permettant de sauvegarder l'état de l'apprentissage à intervalle d'itération réuglière.
        * Ajout à la classe :class:`~dstk.pytorch._callback.EarlyStoppingCallback` du support de l'action keyboard_interrupt (CTR+C) pour récupérer le meilleur modèle après interruption.
        * Ajout à la classe :class:`~dstk.pytorch._callback.PrintCallback` de l'erreur rencontré dans error_interrupt si l'apprentissage s'est arrêté suite à une erreur inconnu.
        * Ajout de la classe :class:`~dstk.pytorch._callback.Supervision` permettant de faire le suivi de modèle via Tensorboard.
        * Amélioration des méthodes de prédictions des classes :class:`~dstk.pytorch._regressor.BaseRegressor`, :class:`~dstk.pytorch._regressor.BaseRegressorOnline`, :class:`~dstk.pytorch._regressor.BaseClassifier" et :class:`~dstk.pytorch._regressor.BaseClassifierOnline`.
        * Adaptation de la classe :class:`~dstk.pytorch._random_search.RandomizedSearchOnline` pour s'adapter au nouveau format de result de cross validation de Scikit-Learn de la version 0.21.
        * Changements importants dans la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit` :
            * Prise en compte de l'intérruption volontaire de l'utisateur pendant l'apprentissage.
            * L'optimizer n'est plus obligatoirement en attribut du modèle et peut être envoyé en argument d'entré de la méthode d'entrainement.
            * Prise en charge des interruptions forcées par CTR+C ou des interruptions involontaire.
            * Meilleur prise en charge de l'entrainement sur les autres supports que CPU.
            * Intégration de AMP à l'emprentissage (Automatic Mixed Precision).
            * Prise en charge de la fonciton d'accumulation du gradient pour simuler des gros batch size si le hardware ne le supporte pas. Cette feature s'acconpagne d'une possibilité de clipper le gradient par une norme max.
            * Séparation de la procédure SWA de l'entraînement de la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit`. Intégration d'une fonction permettant le processus SWA :func:`~dstk.pytorch._swa.swa_fit`.
    * 3.6.5 (19/01/2022)
        * Réctification d'un bug sur test de la divergense quand le :func:`~dstk.pytorch._base.BaseEnvironnement.fit` de PyTorch est fait sur une carte graphique.
        * Déplacement du build dans le :func:`~dstk.pytorch._base.BaseEnvironnement.fit` pour être fait avant l'initialisation de l'optimizer évitant une erreur et la nécessiter d'initiliser la classe une première fois hors du la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit`.
    * 3.7.0 (13/02/2022)
        * Ajout de la classe :class:`~dstk.utils._trie.Trie` permettant de créer et gérer des graphes de type Trie à l'aide de Python vanilla.
    * 3.7.1 (14/02/2022)
        * Ajout de l'attribue :attr:`~dstk.utils._trie.Trie.isleaf` aux Trie permettant de savoir si un noeud est terminal.
        * Propagation de l'attribue default_fields aux différents noeud du graphe.
        * Amélioration de la méthode de classe :func:`~dstk.utils._trie.Trie.deep_init` permettant une initialisation plus saine du Trie à partir d'un objet graphe Python vanilla de type dict.
    * 3.7.2 (12/03/2022)
        * Ajout de la compatibilité des strings dans la classe :class:`~dstk.utils._rolling.RollingWindow`.
    * 3.7.3 (20/05/2022)
        * Ajout de la possibilité de checker si la target et un tensor de la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit` et :class:`~dstk.pytorch._callback.FitControl`.
    * 3.7.4 (25/05/2022)
        * Permet d'intégrer l'argument state de type :class:`~dstk.pytorch._callback.FitState` en entré de la loss fonction à l'aide des mots clefs : 'state', 'fitstate' ou 'fit_state' ou encore le FitState sera intégrer par défaut sous le mot clef state si kargs ou kwargs sont en entrés de la fonction loss.
    * 3.7.5 (27/05/2022)
        * Ajout de fonctionnalités à :class:`~dstk.pytorch._callback.Backup` permettant de garder plusieurs itérations.
        * Changement de comportement par défaut du :class:`~dstk.pytorch._callback.LRSchedulerCallback` avec verbose passant de True à False.
    * 3.7.6 (27/05/2022)
        * :class:`~dstk.pytorch._callback.Backup` supprission des fichiers du plus ancien au plus récent.
    * 3.7.7 (28/05/2022)
        * Ajout de le possibilité de rechergé un état du scheduler du learning rate de la classse :class:`~dstk.pytorch._callback.LRSchedulerCallback`.
        * Ajout de la possibilité de réalisé un backup du learning rate.
    * 3.7.8 (29/05/2022)
        * :func:`~dstk.pytorch._base.BaseEnvironnement.fit` accepter à présent des optimizer déjà instancié à l'extérieur de la méthode fit afin de realiser des instanciation plus spécifique en fonction de la modélisation si nécessaire.
        * :func:`~dstk.pytorch._base.BaseEnvironnement.fit` correction du bug qui ce produisait pour les fonctions loss qui n'était pas des fonctions mais des classes _Loss PyTorch.
        * Modification dans :class:`~dstk.pytorch._callback.LRSchedulerCallback` pour l'extention du backup de 'lsc' en 'lrs'.
        * Suppression du warning dans :func:`~dstk.pytorch._base.BaseEnvironnement.fit` si le CPU est choisit par l'utlisateur et qui indiquait pourtant que CUDA était sélectionné.
    * 3.7.9 (30/05/2022)
        * Ajout de la fonction :func:`~dstk.utils._seed.set_seed` permettant de globaliser l'initialisation des seeds des générateurs pseudo-aléatoires de Python (Python + numpy + PyTorch + Cuda).
        * Résolution du bug des backups lié aux triages des fichiers.
        * Amélioration de la gestion des KeyboardInterrupt dans la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit`.
        * Débug et meilleur gestion des KeyboardInterrupt dans la classe :class:`~dstk.pytorch._callback.EarlyStoppingCallback`.
    * 3.7.10 (02/06/2022)
        * Rectification d'un bug dans les méthodes :func:`~dstk.pytorch._regressor.BaseRegressorOnline.fit` et :func:`~dstk.pytorch._regressor.BaseClassifierOnline.fit` de kargs_dataloader en dataloader_kargs.
    * 3.7.11 (03/06/2022)
        * Addaptation au changement de comportement dans Scikit-Learn pour la classe :class:`~dstk.pytorch._random_search.RandomizedSearchOnline` avec fit_failed -> fit_error.
    * 3.7.12 (05/06/2022)
        * Ajout de la possibilité d'appeler la méthode post_move_to_tup présent dans la classe modèle dans la méthode :func:`~dstk.pytorch._regressor.BaseClassifierOnline.fit` permettant de prendre en charge la laison des poids dans le graphe pour les TPU.
    * 3.7.13 (08/06/2022)
        * Permet de choisir si init de la seed sur cuda et assure que l'initialisation de la seed de numpy est compatible 2**32 dans :func:`~dstk.utils._seed.set_seed`.
    * 3.7.14 (11/06/2022)
        * Correctif dans :func:`~dstk.pytorch._base.BaseEnvironnement.fit` permettant à présent de pouvoir mettre une fonction "partial" dans la loss_fn.
    * 3.7.15 (16/06/2022)
        * Correctir sur la classe :class:`~dstk.data._pandas_sql.PandasSQL` évitant les erreurs pour l'affichage de la classe dans le terminal.
    * 3.7.16 et 3.7.17 (19/06/2022)
        * Résolution d'un bug sur les numpy arrays unidimensionnels dans la classe :class:`~dstk.utils._rooling.RollingWindow`. A présenté elle peut être de n'importe quel d'imensions.
        * Plus de chemin par défaut dans la classe :class:`~dstk.pytorch._callback.Backup` évitant la création d'un répertoire parasite pendant l'initialisation de la classe.
        * Suppression du callback Backup dans les callback par défaut.
    * 3.7.18 (20/06/2022)
        * Permet d'envoyer le tensors sur un device même s'ils sont dans un tuple ou une liste.
    * 3.7.18-19 (20/07/2022)
        * Pour :func:`~dstk.pytorch._base.BaseEnvironnement.save_model` et :func:`~dstk.pytorch._base.BaseEnvironnement.load_weights` ajout la sauvegarde d'attribut récurrent (dans la cas d'héritage de classe par exemple).
    * 3.7.21 (28/07/2022)
        * Ajout de ravel dans :func:`~dstk.pytorch.metrics.apply_metric_error` permettant de pouvoir injecter des prédictions multi-dimensionel.
    * 3.7.22 (29/07/2022)
        * Ajout de la possibilité de pouvoir mettre plusieurs méta data dans la classe :class:`~dstk.pytorch.supervision.EmbeddingProjector`.
    * 3.8.0 (23/10/2022)
        * Intégration des fonctionnalités **Dirstributed Data Parallel** de PyTorch à la classe :class:`~dstk.pytorch._base.BaseEnvironnement` permettant d'entraîner un modèle sur plusieurs noeuds multi-GPU et/ou multi-CPU.
        * Intégration de la fonction :func:`~dstk.pytorch._utils_distrib.auto_init_distributed` permettant de facilité l'initialisation du multi-processing PyTorch.
    * 3.8.1, 3.8.2, 3.8.3 (04/11/2022)
        * Réctification d'assignation des varaibles shuffle et drop_last dans la fonction :func:`~dstk.pytorch._utils_distrib.auto_init_distributed`.
        * Affin de résoudre des problèmes d'import circulaire le callback par défaut de la méthode :meth:`~dstk.pytorch._base.BaseEnvironnement.fit` est None peut être à présent Optionnel.
        * Prise en compte des classes :class:`~dstk.pytorch._regressor.BaseRegressor`, :class:`~dstk.pytorch._regressor.BaseRegressorOnline`, :class:`~dstk.pytorch._regressor.BaseClassifier" et :class:`~dstk.pytorch._regressor.BaseClassifierOnline` pour la distribution PyTorch multi-noeud/multi-device.
    * 3.8.4, 3.8.5, 3.8.6, 3.8.7 (07/11/2022)
        * Posiibilité d'imposer la classe d'héritage à la fonction de surcharge DDP :func:`~dstk.pytorch._utils_distrib.auto_init_distributed`.
        * La méthode de sauvegarde de la classe :class:`~dstk.pytorch._base.BaseEnvironnement` filtre les potentiels paramètres de DDP afin de préserver la compatibilité avec Scikit-Learn.
    * 3.8.8-3.8.17 (08/11/2022)
        * Amélioration de l'interface DPP afin de s'adapter mieux aux spécificités de la classe du modèle de l'utilisateur.
    * 2.8.18 (21/12/2022)
        * Rétification du bug pour le décorrateur :func:`~dstk.metrics._metrics.timer` qui renvoit a présent le temps en milli-seconde.
        * Changement de comportement dans la classe :class:`~dstk.pytorch._callback.LRSchedulerCallback` où le changement de learning rate ce fait au début de l'itération de train et non à la fin (afin de ne plus avoir le Warning dans Pytorch).
        * Changements dans la méthode :meth:`~dstk.pytorch._base.BaseEnvironnement.fit` placant le reset des gradients de l'optimizer avant le setp et mise en place d'une méthode plus générique dans le cas multi-GPU/noeud où a présent le dataset attendu n'est plus obligatoirement un DistributedSampler mais peut être une autre classe (ce qui peut réduire les contraintes d'utilisant dans le cadre des dataset IterableDataset où parfois on est plus au format map et on n'a donc pas de __getitem__).
        * Intégration des informations **is_master** et **is_distributed** dans la classe :class:`~dstk.pytorch._callback.FitControl` permettant une meilleur métrise de l'environnement d'exécusion.
    * 2.8.19, 2.8.20 (20/07/2023)
        * Changement d'héritage de :class:`~dstk.utils._trie.Trie` Dict en OrderedDic afin de préserver l'ordonnance des clefs du trie.
        * Modification du :class:`~dstk.utils._chunck.Chunker` qui permet de chuncker de type itérable au lieu de seulement tuple, list, numpy et Pandas.
    * 2.8.21 (05/01/2024)
        * Réctification d'un bug dans la classe :meth:`~dstk.pytorch._base.BaseEnvironnement.fit` qui empêché le bon déroulement de l'apprentissage.
        * Ajout des la fonction :func:`~dstk.pytorch._utils.send_to` permettant d'envoyer des conteneurs de tensors vers un devices facilement.
        * Ajout des fonctions :func:`~dstk.utils._trie.flat_level_idx_generator`, :func:`~dstk.utils._trie.get_sub_trie_from_idx`, :func:`~dstk.utils._trie.sample_trie`, :func:`~dstk.utils._trie.flat_trie_generator` et :func:`~dstk.utils._trie.number_leaf`, permettant de facilité la manipulation des Tries.

### 2.0

    * Résolutions de bugs diverses.
    * Concept de Callback pour x250.pytorch permettant de rendre la partie entraînement plus modulaire et lisible.
    * Intégration du concept de SWA (Stochastic Weight Averaging) pour rendre les modèles plus robuste à l'inférence.

### 1.0

    * Séparation du template data science et des _utils.py afin d'être intégré au socle Python d'Arkéa.
    * Création du package x250 restructurant les _utils.py.
    * Intégration de l'utilitaire PyTorch permettant de wrapper un réseau profond à Scikit-Learn simplement.

### 0.0

    * Création du squelette template data science.
    * 0.1 :
        * Intégration de fonctions et classes utilitaires au template dans des fichiers _utils.py à différent niveau de la structure du template.

Historique
==========
.. raw:: html
    :file: _static/histo.html

