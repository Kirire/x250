Gestion des aggrégations
========================

.. automodule:: dstk.metrics._aggregator
   :members:
   :undoc-members:
   :show-inheritance:

