Fonctions statistiques aidant à l'analyse des courbes ROC
=========================================================

.. automodule:: dstk.visualization.statistique
   :members:
   :undoc-members:
   :show-inheritance:

