Classe d'analyse de courbes ROC
===============================

.. automodule:: dstk.visualization._roc_analyser
   :members:
   :undoc-members:
   :show-inheritance:

