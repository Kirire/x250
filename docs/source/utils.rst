**utils** c'est quoi ?
======================
Les utilitaires de dstk.utils sont des fonctions ou classes pouvant être utilisées partout dans un projet. Elles n'ont pas de teinte purement ML, features engineering, etc. On y trouvera pêle-mêle :
    - :func:`~dstk.utils._check.check_dataframe` permettant de checker si la variable est une DataFrame Pandas et également de checker la présence de certaines colonnes ;
    - :class:`~dstk.utils._chunk.Chunker` classe permettant de créer un chunker pour traiter tronçon par tronçon un itérable (utile également pour paralléliser des longs processus) ;
    - :class:`~dstk.utils._trie.Trie` classe permettant de créer un graphe de type arbre trie et facilitant sa manipulation ;
    - :func:`~dstk.utils._set_params_deep.set_params_deep` permettant de modifier une sous-partie des paramètres d'une pipeline Scikit-Learn ;
    - :func:`~dstk.utils._statistique.weighted_avg_and_std` permettant de calculer la moyenne et l'écart-type pondérés ; 
    - *etc.*
Et beaucoup d'autres méthodes.

.. note:: Numpy est équipé d'une fonction permettant de calculer une moyenne pondérée, **mais pas un écart-type pondéré**.

**utils** : classes et fonctions
================================

.. toctree::
   :maxdepth: 2

   utils/alls

