Random search pour approche online
==================================

Il existe dans Scikit-Learn plein de statégies de technique de `cross-validation`_, mais aucune qui répondent aux besoins d'une approche online. En effet, **l'ensemble des stratégies de Scikit-Learn sont de type batch**. Le module :mod:`~dstk.pytorch._random_search` au travers de la classe :class:`~dstk.pytorch._random_search.RandomizedSearchOnline` permet de répondre aux besoins des approches online. :class:`~dstk.pytorch._random_search.RandomizedSearchOnline` permet de faire une estimation de la performance du réseau par estimation de Monte-Carlo. Le nombre d'itération pour l'estimation de Monte-Carlo est contrôlée par l'argument *monte_carloint*.

.. _cross-validation: https://scikit-learn.org/stable/modules/cross_validation.html

.. warning:: Pour le bon déroulement du processus d'estimation des meilleurs paramètres il est indispensable que le réseau soit initialisé aléatoirement.

.. automodule:: dstk.pytorch._random_search
   :members:
   :undoc-members:
   :show-inheritance:

