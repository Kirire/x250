Base des estimateurs PyTorch
============================
Les classes :class:`~dstk.pytorch._classifier.BaseClassifier`, :class:`~dstk.pytorch._classifier.BaseRegressor` et leurs équivalentes online :class:`~dstk.pytorch._classifier.BaseClassifierOnline`, :class:`~dstk.pytorch._classifier.BaseRegressorOnline` constituent le fondement de la libraire dstk.pytorch. Ces classes doivent être héritées d'un module PyTorch afin de rendre compatible le réseau Sciki-Learn. Elles portent en elles une méthodes d'apprentissage avec des fonctionnalités telque le *early stopping* le processsus d'apprentissage SAW, etc, une gestions des sauvegardes du réseau et chargement du modèle simple, etc.

.. automodule:: dstk.pytorch._base
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dstk.pytorch._classifier
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: dstk.pytorch._regressor
   :members:
   :undoc-members:
   :show-inheritance:

