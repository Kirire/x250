Stochastic Weight Averaging (SWA)
=================================
Processus SWA utilisé dans la méthode d'apprentissage du modèle :func:`~dstk.pytorch._base.BaseEnvironnement.fit`. Pour plus de détails sur la procédure SWA se référer à `l'article`_.

.. image:: ../images/pytorch/swapytorch.png
    :width: 700
    :align: center
    :alt: Représentation synthétique de la procedure SWA

.. _l'article: https://arxiv.org/abs/1803.05407

.. automodule:: dstk.pytorch._swa
   :members:
   :undoc-members:
   :show-inheritance:

