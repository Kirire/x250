Exemples d'un regresseur online
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. literalinclude:: ../../../examples/pytorch/regressor_online_example.py

.. image:: ../images/examples/pytorch_reg_online.png
    :width: 700
    :align: center
    :alt: Image d'un régresseur online entraîné via BaseRegressorOnline

