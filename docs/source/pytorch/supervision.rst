Outils de supervision TensorBoard
=================================

.. automodule:: dstk.pytorch.supervision
   :members:
   :undoc-members:
   :show-inheritance:

