Callbacks
=========
Classes permettant de contrôler et maîtriser la phase d'apprentissage de la méthode :func:`~dstk.pytorch._base.BaseEnvironnement.fit`. Intègre aussi les classes :class:`~dstk.pytorch._callback.FitState` et :class:`~dstk.pytorch._callback.FitControl` permettant de gérer le processus d'apprentissage. Via la classe interface :class:`~dstk.pytorch._callback.CallbackInterface` il est très facile de créer un callback par l'utilisateur pour implémenter de nouvelles possibilités.

.. automodule:: dstk.pytorch._callback
   :members:
   :undoc-members:
   :show-inheritance:

