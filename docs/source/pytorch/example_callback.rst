Exemple d'un callback utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. literalinclude:: ../../../examples/pytorch/tensorboard_callback.py

.. image:: ../images/examples/tensorboard_callback.png
    :width: 600
    :align: center
    :alt: Callback TensorBoard

