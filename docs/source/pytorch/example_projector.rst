Exemple pour visualiser un embedding via TensorBoard
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. literalinclude:: ../../../examples/pytorch/projector_example.py

.. image:: ../images/examples/projector_2.png
    :width: 600
    :align: center
    :alt: Capture d'écran TensorBoard pour un t-SNE

.. image:: ../images/examples/projector_3.png
    :width: 600
    :align: center
    :alt: Capture d'écran TensorBoard pour un UMAP

