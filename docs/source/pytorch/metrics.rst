Métriques pour les approches online
===================================

.. automodule:: dstk.pytorch.metrics
   :members:
   :undoc-members:
   :show-inheritance:

