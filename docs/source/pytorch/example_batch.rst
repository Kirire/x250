Exemple d'un classifieur batch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. literalinclude:: ../../../examples/pytorch/classifier_example.py

.. image:: ../images/examples/pytorch_classifier_example.png
    :width: 600
    :align: center
    :alt: Image d'un classifieur bi-classe entraîné via BaseClassifier

