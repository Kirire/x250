**ssh** c'est quoi ?
====================
Le module dstk.ssh permet de créer un client SSH vers un serveur distant. Il permet diverses fonctionnalités, comme par exemple :
    - permettre de faire des SCP (Secure CoPy) de fichiers ou répertoires locaux vers le serveur ;
    - d'éxécuter des commandes sheel sur le serveur distant ;
    - de lancer simplement des scripts pig sur la cellule Hadoop ;
    - de pousser et descendre des fichiers sur la cellule Hadoop depuis le serveur distant ;
    - ...
L'avantage de ces classes et fonctions est de pouvoir réaliser un projet de data science dans un espace de travail cohérent (entièrement sur une machine) sans avoir à passer d'une machine à l'autre pour faire les opérations d'extractions.

.. warning:: Il est fortement conseillé de mettre les résultats d'une extraction pig dans le répertoire **staging** de la cellule Hadoop, de récupérer l'URL à l'aide de la méthode :func:`~dstk.ssh._ssh.ClientSSH.get_url_staging` de la classe ClientSSH et de récupérer les données directement en local via la fonction :func:`~dstk.ssh._utils.download_file`.

**ssh** : classes et fonctions
==============================

.. toctree::
   :maxdepth: 2

   ssh/ssh
   ssh/utils

