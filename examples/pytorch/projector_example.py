#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exemple d'utilisation du EmbeddingProjector

Created on Sun Mar 22 13:55:33 2020

@author: Cyrile Delestre
"""

from sklearn.datasets import load_digits

from dstk.pytorch.supervision import EmbeddingProjector

if __name__=='__main__':
    # Chargement du dataset toy de MNIS Logits
    X, y = load_digits(return_X_y=True)

    # Transformation en image chaque observation
    image = list(map(lambda x: x.reshape(8, 8), X))

    # Instenciation de EmbeddingProjector via PyTorch et création du logger
    # TensorBoard
    proj=EmbeddingProjector(
        X=X,
        y=y,
        image=image,
        kargs_writer=dict(log_dir="./tensorboard/tsne")
    )

    # Push dans le logging TensorBoard
    proj.push()

    print(
        "Il reste à présent à ouvir TensorBoard dans le bon répertoire. Si"
        "vous n'avez pas encore installé TensorBoard il suffit de l'installer "
        "via le terminal avec la commande :\n"
        "    >>> pip install tensorboard\n"
        "Si TensorBoard s'est installé convenablement il suffit d'ouvrir un "
        "terminal et d'aller dans le répertoire courant et de taper :\n"
        "    >>> tensorboard --logdir ./tensorboard\n"
        "Ensuite d'ouvrir un navigateur web et d'aller sur le chemin "
        "indiqué par TensorBoard, puis une fois sur la page TensorBoard "
        "dans le menu en haut à droite de la page aller dans Projector et "
        "de choisir une méthode de projection : PCA, t-SNE, UMAP, etc. le "
        "tout en temps réel. Idéal pour analyser l'information contenu dans "
        "un dataset ;-)"
    )
